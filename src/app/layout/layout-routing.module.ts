import { NgModule } from '@angular/core';
import { Routes, RouterModule,CanActivate } from '@angular/router';
import { GuardService as AuthGuard } from '../services/guard.service';

import { LayoutComponent } from './layout.component';
import { KbcComponent } from './kbc/kbc.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
import { AudioScreenComponent } from './audio-screen/audio-screen.component';
import { KeyportfolioEntryComponent } from '../core-components/keyportfolio-entry/keyportfolio-entry.component';
import { AppoinmentsComponent } from './appoinments/appoinments.component';
import { PhotobothComponent } from './photoboth/photoboth.component';



const routes: Routes = [
  { path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      // {path: 'index', loadChildren: ()=> import('../core-components/index/index.module').then(m => m.IndexModule)},
      {path: 'lobby', loadChildren: ()=> import('../core-components/lobby/lobby.module').then(m => m.LobbyModule)},
      { path: 'hallway', loadChildren: () => import('../core-components/hallway/hallway.module').then(m => m.HallwayModule) },
      {path: 'welcome', loadChildren: ()=> import('../core-components/welcome-lobby/welcome-lobby.module').then(m => m.WelcomeLobbyModule)},
      {path: 'auditorium', loadChildren: ()=> import('../core-components/auditorium/auditorium.module').then(m => m.AuditoriumModule)},
      {path: 'exhibitionHall', loadChildren: ()=> import('../core-components/exhibition-hall/exhibition-hall.module').then(m => m.ExhibitionHallModule)},
      {path: 'registrationDesk', loadChildren: ()=> import('../core-components/registration-desk/registration-desk.module').then(m => m.RegistrationDeskModule)},
      {path: 'networkingLounge', loadChildren: ()=> import('../core-components/networking-lounge/networking-lounge.module').then(m => m.NetworkingLoungeModule)},
      {path: 'stage', loadChildren: ()=> import('../core-components/stage/stage.module').then(m => m.StageModule)},
      {path: 'meetingRoom', loadChildren: ()=> import('../core-components/meeting-room/meeting-room.module').then(m => m.MeetingRoomModule)},
      {path: 'chats', loadChildren: ()=> import('../core-components/chat/chat.module').then(m => m.ChatModule)},
      {path: 'myContacts', loadChildren: ()=> import('../core-components/my-contacts/my-contacts.module').then(m => m.MyContactsModule)},
      {path: 'briefcase', loadChildren: ()=> import('../core-components/briefcase/briefcase.module').then(m => m.BriefcaseModule)},
      {path: 'keyportfolio', loadChildren: ()=> import('../core-components/key-portfolio/key-portfolio.module').then(m => m.KeyPortfolioModule)},
      {path:'quiz', component:KbcComponent},
      {path:'capturePhoto', component:CapturePhotoComponent},
      {path:'capturePhoto2', component:PhotobothComponent},
      {path: 'audioScreen', component:AudioScreenComponent},
      {path:'entryKeyportfolio', component:KeyportfolioEntryComponent},
      {path:'appointments', component:AppoinmentsComponent},

    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
