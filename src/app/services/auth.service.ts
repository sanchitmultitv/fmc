import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from './apiConfig/api.constants';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;
  signup = ApiConstants.signup;
  login = ApiConstants.login;
  constructor(private http: HttpClient) { }
  
  register(data: any){
    const token = 123;
    return this.http.post(`${this.baseUrl}/${this.signup}/${token}`, data);
  }

  loginMethod(email,pwd){
    // console.log(loginObj+"YESS")
    return this.http.get(`${this.baseUrl}/${this.login}/event_id/140/email/${email}/pwd/${pwd}`);
  }

  logout(user_id):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.logout}/user_id/${user_id}`);
  }
  acmeLoggedinMethod(loginObj:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.acmeLogin}/event_id/${loginObj.event_id}/email/${loginObj.email}/registration_number/${loginObj.registration_number}`);
  }
}
