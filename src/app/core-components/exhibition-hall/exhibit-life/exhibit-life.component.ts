import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-exhibit-life',
  templateUrl: './exhibit-life.component.html',
  styleUrls: ['./exhibit-life.component.scss']
})
export class ExhibitLifeComponent implements OnInit {
  liveMsg=false;
  constructor( private router: Router, private chat: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
   // this.chat.getconnect('toujeo-60');
    // this.chat.getMessages().subscribe((data=>{
    //   console.log('data',data);
    //   if(data == 'start_live'){
    //     this.liveMsg = true;
    //   }
    //   if(data == 'stop_live'){
    //     this.liveMsg = false;
    //   }
     
    // }));
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', '140');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  gotoCare(){
 this.router.navigate(['/exhibitionHall/care']);
  }
  gotoCommitToSucceed(id){
    this.router.navigate(['/exhibitionHall/commitToSucceed', id]);
  }
  gotoDareToInnovative(id){
    this.router.navigate(['/exhibitionHall/dareToInnovative', id]);
  }
  gotoGrowBySharing(){
    this.router.navigate(['/exhibitionHall/growBySharing']);
  }
}
